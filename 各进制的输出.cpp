#include<iostream>
#include<bitset>
using namespace std;
int main()
{
	cout << bitset<8>(0b00101011) << endl;//二进制输出
	cout << oct << 0013 << endl;//八进制输出  前面的零会省略
	cout << hex << 0x0b << endl;//十六进制输出
	return 0;
}