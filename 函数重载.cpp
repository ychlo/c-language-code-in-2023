#include<iostream>
using namespace std;//函数重载，静态多态，编译时确定函数的入口地址（静态联编）
int add(int a)
{
	return a;
}
int add(int a,int b)
{
	return a + b;
}
int add(int a,int b,int c)
{
	return a + b + c;
}
int main()//函数名相同而参数不同可构成函数重载
{         //参数类型不同
	      //参数顺序不同
	     //参数数量和参数类型均不同
	cout << add(1) << endl;
	cout << add(1, 2) << endl;
	cout << add(1, 2, 3) << endl;
	return 0;
}