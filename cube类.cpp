#include<iostream>
using namespace std;
class cube
{
private:
	float mylong;
	float mywide;
	float myhigh;
public:
	void Initcube()
	{
		mylong = 0;
		mywide = 0;
		myhigh = 0;
	}
	float Getlong()
	{
		return mylong;
	}
	float Getwide()
	{
		return mywide;
	}
	float Gethigh()
	{
		return myhigh;
	}
	void setlong(float a)
	{
		mylong = a;
	}
	void setwide(float b)
	{
		mywide = b;
	}
	void sethigh(float c)
	{
		myhigh = c;
	}
	void showcube()
	{
		cout << "long:" << mylong << endl;
		cout << "wide:" << mywide << endl;
		cout << "high:" << myhigh << endl;
	}
	float V()
	{
		return mylong * mywide * myhigh;
	}
	float mianji()
	{
		return mylong * mywide * 2 + mylong * myhigh * 2 + mywide * myhigh * 2;
	}
};
int main(int argc, char* argv[])
{
	cube a;
	a.Initcube();
	a.showcube();
	a.sethigh(3);
	a.setwide(4);
	a.setlong(5);
	cout<<a.Gethigh()<<endl;
	cout << a.Getlong() << endl;
	cout << a.Getwide() << endl;
	a.showcube();
	cout << a.V() << endl;
	cout << a.mianji() << endl;
	return 0;
}