#include<iostream>
#include<math.h>
#include<stdlib.h>
using namespace std;
int n, m;
char arr[21][21];
bool vis[26];
int maxa;
int dx[] = { 0,0,-1,1 };
int dy[] = { -1,1,0,0 };
void dfs(int x, int y, int dep)
{
	maxa = max(maxa, dep);
	for (int i = 0; i < 4; i++)
	{
		int bx = x + dx[i];
		int by = y + dy[i];
		if (bx<1 || bx>n || by<1 || by>n)
		{
			continue;
		}
		if (vis[arr[bx][by] - 'A'] == 1)
		{
			continue;
		}
		vis[arr[bx][by] - 'A'] = 1;
		dfs(bx, by, dep + 1);
		vis[arr[bx][by] - 'A'] = 0;
	}
}
int main()
{
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= m; j++)
		{
			cin >> arr[i][j];
		}
	}
	vis[arr[1][1] - 'A'] = 1;
	dfs(1, 1, 1);
	cout << maxa;
	return 0;
}