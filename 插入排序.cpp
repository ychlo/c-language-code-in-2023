#include<iostream>
using namespace std;
void insertsort(int arr[], int n)
{
	for (int i = 1; i < n; i++)
	{
		int tmp = arr[i];
		int j = i;
		while (j > 0 && tmp < arr[j - 1])
		{
			arr[j] = arr[j - 1];
			j--;
		}
		arr[j] = tmp;
	}
}
int main()
{
	int a[10] = { 23,11,32,44,12,34,67,12,35,10 };
	int length = sizeof(a) / sizeof(a[0]);
	insertsort(a, length);
	for (int i = 0; i < length; i++)
	{
		cout << a[i] << " ";
	}
	return 0;
}