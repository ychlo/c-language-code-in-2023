#include<iostream>
using namespace std;
struct a
{
	char s;
	double d;
};
int main()
{
	cout << sizeof(a) << endl;
	return 0;
}

//16
//第一个成员存放在偏移量为0的地址处
//&a与&a.s是等价的
//其他成员要对齐到某个数字（对齐数：编译器（8）设置的默认对齐数与该成员大小的较小值）的整数倍的地址，
//结构体总大小为最大对齐数的整数倍
//#pragma pack(num)可预设系统默认对齐数