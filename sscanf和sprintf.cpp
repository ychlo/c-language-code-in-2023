#pragma warning(disable : 4996)
#include<stdio.h>
struct s
{
	char arr[10];
	int age;
	float f;
};
int main()
{
	struct s S = {"HELLO",10,1.5};
	struct s tmp = { 0 };
	char buf[100];

	sprintf(buf, "%s %d %f", S.arr, S.age, S.f);//将各种类型的数据转换为字符串
	printf("%s", buf);
	sscanf(buf, "%s %d %f", tmp.arr, &(tmp.age), &(tmp.f));
	printf("%s %d %f", tmp.arr, tmp.age, tmp.f);
	return 0;
}