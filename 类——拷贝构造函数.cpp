#include<iostream>
using namespace std;
class Data
{
private:
	int a;
public:
	Data()
	{
		a = 0;
		cout << "无参构造" << endl;
	}
	Data(int A)
	{
		a = A;
		cout << "有参构造" <<"a="<<a << endl;
	}
	Data(const Data &ob1)//拷贝构造
	{
		a = ob1.a;
		cout << "拷贝构造函数" << endl;
	}
	~Data()
	{
		cout << "析构函数" << endl;
	}
};
int main(int argc, char* argv[])
{
	Data ob1(10);
	Data ob2 = ob1;//拷贝构造函数，是一种特殊的构造函数
	return 0;
}