#include<iostream>
using namespace std;
#define N 101
char a[N][N];
char b[N][N];
long long ans;
int dx[] = { 0,0,-1,1 };
int dy[] = { -1,1,0,0 };
int main()
{
	int n, m;
	cin >> n;
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			cin >> a[i][j];
			b[i][j] = a[i][j];
		}
	}
	cin >> m;
	for (int k = 2; k <= m; k++)
	{
		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < n; j++)
			{
				if (a[i][j] == '@')
				{
					for (int l = 0; l < 4; l++)
					{
						int bx = i + dx[l];
						int by = j + dy[l];
						if (bx >= 0 && bx < n && by >= 0 && by < n && b[bx][by] == '.')
						{
							b[bx][by] = '@';
						}
					}
				}
			}
			
		}
		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < n; j++)
			{
				a[i][j] = b[i][j];
			}
		}
	}
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			if (a[i][j] == '@')
			{
				ans++;
			}
		}
	}
	cout << ans;
	return 0;
}