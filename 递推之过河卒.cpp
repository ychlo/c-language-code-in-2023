#include<iostream>
using namespace std;
long long f[23][23];
bool vis[23][23];//标记是否能走
int macx[] = { 1,1,-1,-1,2,2,-2,-2 };
int macy[] = { 2,-2,2,-2,1,-1,1,-1 };//设置马控制的范围
int main()
{
	int mx, my,n,m;
	cin >> n >> m >> mx >> my;
	vis[mx][my] = 1;
	for (int i = 0; i < 8; i++)
	{
		int bx = macx[i] + mx;
		int by = macy[i] + my;
		if (bx >= 0 && bx <= n && by >= 0 && by <= m)
		{
			vis[bx][by] = 1;
		}
	}
	f[0][0] = 1;
	if (vis[0][0] == 1)
		f[0][0] = 0;
	for (int i = 1; i <= n; i++)
	{
		f[i][0] = f[i - 1][0];
		if (vis[i][0] == 1)
		{
			f[i][0] = 0;
		}
	}
	for (int i = 1; i <= m; i++)
	{
		f[0][i] = f[0][i - 1];
		if (vis[0][i] == 1)
		{
			f[0][i] = 0;
		}
	}
	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= m; j++)
		{
			f[i][j] = f[i - 1][j] + f[i][j - 1];
			if (vis[i][j] == 1)
			{
				f[i][j] = 0;
			}
		}
	}
	cout << f[n][m] << endl;
	return 0;
}