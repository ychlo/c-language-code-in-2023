#include<iostream>
#include<assert.h>
using namespace std;
int my_strcmp(const char* a, const char* b)
{
	assert(a && b);
	while (*a==*b)
	{
		if (*a == '\0')
		{
			return 0;
		}
		a++;
		b++;
	}
	return *a - *b;
}
int main()
{
	const char* a = "abcde";
	const char* b = "abcd";
	int c = my_strcmp(a, b);
	if (c < 0)
	{
		cout << "a<b" << endl;
	}
	else if (c > 0)
	{
		cout << "a>b" << endl;
	}
	else
	{
		cout << "a=b" << endl;
	}
	return 0;
}