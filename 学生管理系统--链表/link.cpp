#include"link.h"
void menu()
{
	cout << "**********************" << endl;
	cout << "  a.打印菜单          " << endl;
	cout << "  b.插入元素（头部)   " << endl;
	cout << "  c.插入元素（尾部)   " << endl;
	cout << "  d.插入元素 (顺序)   " << endl;
	cout << "  e.打印元素的信息    " << endl;
	cout << "  f.查找(按学号)      " << endl;
	cout << "  g.查找(按名字)      " << endl;
	cout << "  h.删除(按学号)      " << endl;
	cout << "  i.删除(按名字)      " << endl;
	cout << "  j.清空所有信息      " << endl;
	cout << "  k.清屏              " << endl;
	cout << "  l.退出              " << endl;
}
//头部插入
stu* insertT(stu* head,stu tmp)
{
	//从堆区申请动态内存
	stu* a = new stu;
	*a = tmp;
	a->next = NULL;
	//判断链表是否为空
	if (head == NULL)
	{
		head = a;
	}
	else
	{
		a->next = head;
		head = a;
	}
	return head;
}
//尾部插入
stu* insertW(stu* head, stu newstu)
{
	//从堆区申请动态内存
	stu* a = new stu;
	*a = newstu;
	a->next = NULL;
	//判断链表是否为空
	if (head == NULL)
	{
		head = a;
	}
	else
	{
		stu* b= head;
		while (b->next != NULL)
			b = b->next;
		b->next = a;
	}
	return head;
}
//有序插入
stu* insertX(stu* head, stu tmp)
{
	//从堆区申请动态内存
	stu* a = new stu;
	*a = tmp;
	a->next = NULL;
	//判断链表是否为空
	if (head == NULL)
	{
		head = a;
	}
	else
	{
		stu* b = head;
		stu* c = head;
		while (c->num < a->num && c->next != NULL)
		{
			b = c;
			c = c->next;
		}
		if (c->num >= a->num)//头部与中部插入
		{
			if (c == head)
			{
				a->next = head;
				head = a;
			}
			else
			{
				b->next = a;
				a->next = c;
			}
		}
		else
		{
			c->next = a;
		}
	}
	return head;
}
//打印
void print(stu* head)
{
	if (head == NULL)
	{
		cout << "该链表为空，无法输出" << endl;
		return;
	}
		stu* pi = head;
		while (pi!= NULL)
		{
			cout << pi->num << " " << pi->name << endl;
			pi = pi->next; 
		}
	return;
}
//按学号查询
void searchxh(stu* head, int xh)
{
	if (head == NULL)
	{
		cout << "该链表为空,无法查询" << endl;
		return;
	}
	stu* a=head;
	while ((a->num!=xh) && (a->next != NULL))
		a = a->next;
	if (a->num == xh)
	{
		cout << "学号：" << a->num << "姓名:" << a->name << endl;
		return;
	}
	else
	{
		cout << "查无此人" << endl;
	}
}
//按姓名查询
void searchxm(stu* head, char* arr)
{
	if (head == NULL)
	{
		cout << "该链表为空,无法查询" << endl;
		return;
	}
	stu* a = head;
	while ((!strcmp(a->name, arr) && (a->next != NULL)))
	{
		a = a->next;
	}
	if (!strcmp(a->name, arr))
	{
		cout << "学号:" << a->num << "姓名：" << a->name << endl;
		return;
	}
	else
	{
		cout << "查无此人" << endl;
	}
}
//按学号删除
stu* deletexh(stu* head, int xh)
{
	stu* a;
	stu* b = NULL;
	if (head == NULL)
	{
		cout << "此表为空，无法删除" << endl;
		return NULL;
	}
	a = head;
	while ((a->num != xh) && (a->next != NULL))
	{
		b = a;
		a = a->next;
	}
	if (a->num == xh)
	{
		if (a == head)
		{
			head = head->next;
			delete a;
		}
		else
		{
			b->next = a->next;
			delete a;
		}
	}
	else
	{
		cout << "找不到删除对象" << endl;
	}
	return head;
}
//按姓名删除
stu* deletexm(stu* head, char* arr)
{
	if (head == NULL)
	{
		cout << "链表不存在" << endl;
		return head;
	}
	stu* a = head;
	stu* b = NULL;
	while ((strcmp(arr, a->name) != 0) && (a->next != NULL))
	{
		b = a;
		a = a->next;
	}
	if ((strcmp(arr, a->name) == 0))
	{
		if (a == head)
		{
			head = head->next;
			delete a;
		}
		else
		{
			b->next = a->next;
			delete a;
		}
	}
	else
	{
		cout << "查无此人" << endl;
	}
	return head;
}
stu* freeb(stu* head)
{
	if (NULL == head)
	{
		cout << "该链表为空" << endl;
		return 0;
	}
	stu* a = head;
	while (a != NULL)
	{
		head = head->next;
		delete a;
		a = head;
	}
	return head;
}