#pragma once
#include<iostream>
using namespace std;
//定义链表节点类型
struct stu
{
	int num;
	char name[30];
	stu* next;
};
void menu();
stu* insertT(stu* a,stu b);
stu* insertW(stu* a,stu b);
stu* insertX(stu* a, stu b);
void print(stu* head);
void searchxh(stu* head, int xh);
void searchxm(stu* head, char* arr);
stu* freeb(stu* head);
stu* deletexh(stu* head, int xh);
stu* deletexm(stu* head, char* arr);