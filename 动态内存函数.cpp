#include<iostream>
#include<assert.h>
using namespace std;
int main()
{
	int* p = (int*)malloc(10 * sizeof(int));//在堆区开辟空间
	cout << p << endl;
	free(p);//回收空间
	p = NULL;//防止非法访问内存
	int* ptr = (int*)calloc(10, sizeof(int));//在堆区开辟一块10个int类型的内存；且全部初始化为0
	realloc(ptr, 20* sizeof(int));//调整空间大小
	return 0;
}