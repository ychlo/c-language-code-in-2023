#include<iostream>
using namespace std;
char arr[102][102];
bool vis[102][102];
int dx[] = { 0,0,-1,1 };
int dy[] = { -1,1,0,0 };
int n, cx, cy, zx, zy,t;
bool flag = false;
void dfs(int x, int y)
{
	if (x == zx && y == zy)
	{
		flag = true;
		return;
	}
	for (int i = 0; i < 4; i++)
	{
		 int bx = x + dx[i];
		 int by = y + dy[i];
		if (vis[bx][by] == 1)continue;
		if (bx<0 || bx>=n || by<0 || by>=n)continue;
		if (arr[bx][by] == '#')continue;
		vis[bx][by] = 1;
		dfs(bx, by);
	}
}
int main()
{
	cin >> t;
	while (t--)
	{
		cin >> n;
		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < n; j++)
			{
				cin >> arr[i][j];
			}
		}
		memset(vis, 0, sizeof(vis));
		flag = 0;
		cin >> cx >> cy >> zx >> zy;
		vis[cx][cy] = 1;
		dfs(cx, cy);
		cout << (flag ? "YES\n" : "NO\n");
	}
	return 0;
}