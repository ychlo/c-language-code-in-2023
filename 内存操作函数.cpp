#include<iostream>
#include<assert.h>
using namespace std;
void* my_memcpy(void* dest, const void* src, size_t num)
{
	assert(dest && src);
	void* a = dest;
	for (int i = 0; i < num; i++)
	{
		*(char*)dest = *(char*)src;
		dest=(char*)dest+1;
		src = (char*)src + 1;
	}
	return a;
}
void* my_memmove(void* dest, const void* src, size_t num)
{
	assert(dest && src);
	void* ret = dest;
	if (dest > src)
	{
		//从后往前拷贝
		while (num--)
		{
			*((char*)dest + num) = *((char*)src + num);
		}
	}
	else
	{
		while (num--)
		{
			*(char*)dest = *(char*)src;
			dest = (char*)dest + 1;
			src = (char*)src + 1;
		}
	}
	return ret;
}
int main()
{
	int arr1[10] = { 1,2,3,4,5,6,7,8,9,10 };
	int arr2[20] = { 0 };
	memcpy(arr2, arr1,20);//内存拷贝函数，20为字节数；
	my_memcpy(arr2, arr1, 20);//自己创建的拷贝函数；
	memmove(arr1 + 2, arr1, 20);//内存移动函数；
	my_memmove(arr1, arr1+2, 20);
	memcmp(arr1, arr2, 4);//内存比较函数
	memset(arr1, 1, 40);
	for (int i = 0; i < 10; i++)
	{
		cout << arr1[i] << endl;
	}
	return 0;
}