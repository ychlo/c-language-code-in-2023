#include<iostream>
using namespace std;
int n;
const int N = 103;
int mp[N][N];
bool vis[N][N];
bool flag = false;
int dx[4] = { 0,0,-1,1 };
int dy[4] = { -1,1,0,0 };
int cx, cy, zx, zy;
bool check(int x, int y)
{
	if (x > 0 && x <= n && y > 0 && y <= n && vis[x][y] != 1 && mp[x][y] == 0)
	{
		return 1;
	}
	return 0;
}
void dfs(int x, int y)
{
	if (vis[x][y] || flag)
		return;
	vis[x][y] = 1;
	if (x == zx && y == zy)
	{
		flag = 1;
		return;
	}
	for (int i = 0; i < 4; i++)
	{
		int nx = x + dx[i];
		int ny = y + dy[i];
		if (check(nx, ny))
		{
			dfs(nx, ny);
		}
	}

}
int main(int argc, char* argv[])
{

	cin >> n;
	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= n; j++)
		{
			cin >> mp[i][j];
		}
	}
	cin >> cx >> cy >> zx >> zy;
    if(mp[cx][cy]==1)
    {
        cout<<"no"<<endl;
    }
	dfs(cx, cy);
	puts(flag ? "yes" : "no");
	return 0;
}
