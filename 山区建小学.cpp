#include<iostream>
#include<math.h>
using namespace std;
int dis[501][501];
int dp[501][501];
int d[501];
int cal(int x, int y)
{
	int sum = 0;
	int mid = (x + y) / 2;
	for (int i = x; i <= y; i++)
	{
		sum += abs(d[i] - d[mid]);
	}
	return sum;
}
int main()
{
	int m, n;
	cin >> m >> n;
	d[1] = 0;
	for (int i = 2; i <= m; i++)
	{
		cin >> d[i];
		d[i] = d[i - 1] + d[i];
	}
	//建立一所小学
	for (int i = 1; i <= m; i++)
	{
		for (int j = i; j <= m; j++)
		{
			dis[i][j] = cal(i, j);
		}
	}
	//建立n所小学
	memset(dp, 0x3f, sizeof(dp));
	for (int i = 1; i <= m; i++)
	{
		for (int j = 1; j <= n && j <=i; j++)
		{
			if (j == 1)
			{
				dp[i][j] = dis[1][i];
			}
			for (int k = 1; k < i; k++)
			{
				dp[i][j] = min(dp[k][j - 1] + dis[k + 1][i],dp[i][j]);
			}
		}
	}
	cout << dp[m][n] << endl;
	return 0;
}