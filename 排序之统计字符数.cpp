#include<iostream>
using namespace std;
char str[1005];
int arr[26];
int main()
{
	cin >> str;
	int lena = strlen(str);
	for (int i = 0; i < lena; i++)
	{
		arr[str[i]-'a']++;
	}
	int max = arr[0];
	for (int i = 1; i < 26; i++)
	{
		if (arr[i] > max)
		{
			max = arr[i];
		}
	}
	for (int i = 0; i < 26; i++)
	{
		if (arr[i] == max)
		{
			cout << char(i + 'a') << " " << max << endl;
			return 0;
		}
	}
}