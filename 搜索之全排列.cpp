#include<iostream>
using namespace std;
string s;
char arr[6];
bool vis[6];
void dfs(int dep)
{
	if (dep == s.size() + 1)
	{
		for (int i = 1; i < s.size() + 1; i++)
		{
			printf("%c ", arr[i]);
		}
		printf("\n");
		return;
	}
	for (int i = 0; i < s.size(); i++)
	{
		if (vis[i] == 0)
		{
			vis[i] = 1;
			arr[dep] = s[i];
			dfs(dep + 1);
			vis[i] = 0;
		}
	}
}
int main(int argc,char* argv[])
{
	cin >> s;
	dfs(1);
	return 0;
}