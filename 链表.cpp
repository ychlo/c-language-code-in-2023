#include<iostream>
using namespace std;
struct stu
{
	int num;
	char name[20];
	struct stu* next;
};
int main()
{
	struct stu node1 = { 10,"李华",NULL };
	struct stu node2 = { 20,"赵大",NULL };
	struct stu node3 = { 30,"亚瑟",NULL };
	struct stu node4 = { 40,"阿瑟",NULL };
	struct stu node5 = { 50,"约",NULL };
	struct stu* head = &node1;
	node1.next = &node2;
	node2.next = &node3;
	node3.next = &node4;
	node4.next = &node5;
	node5.next = NULL;
	struct stu* pb = head;
	while (pb!= NULL)
	{
		cout << pb->name << pb->num << endl;
		pb = pb->next;
	}
	cout << "输出完毕" << endl;
	return 0;
}