#include<iostream>
using namespace std;
int fbnq(int n)
{
	if (n == 0 || n == 1)
	{
		return 1;
	}
	return fbnq(n - 1) + fbnq(n - 2);
}
int main(int argc, char* argv[])
{
	int m;
	cin >> m;
	while (m--)
	{
		int result;
		int a;
		cin >> a;
		result = fbnq(a-1)%1000;
		cout << result << endl;
	}
	return 0;
}