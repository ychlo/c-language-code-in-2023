#include<iostream>
using namespace std;
int arr[22];
bool vis[22];
int n, r;
void dfs(int dep)
{
	if (dep == r + 1)
	{
		for (int i = 1; i < dep; i++)
		{
			printf("%d", arr[i]);
		}
		printf("\n");
		return;
	}
	for (int i = arr[dep-1]+1; i <= n; i++)
	{
		if (vis[i] == 0)
		{
			vis[i] = 1;
			arr[dep] = i;
			dfs(dep + 1);
			vis[i] = 0;
		}
	}
}
int main()
{
	cin >> n >> r;
	dfs(1);
	return 0;
}