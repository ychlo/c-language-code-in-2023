#include<iostream>
#include<assert.h>
using namespace std;
char* my_strstr(const char* str1, const char* str2)
{
	assert(str1 && str2);
	const char* s1 = NULL;
	const char* s2 = NULL;
	const char* cp = str1;
	if (*str2 == '\0')
	{
		return (char*)str1;
	}
	while (*cp)
	{
		s1 = cp;
		s2 = str2;
		while (*s1 && *s2 && (*s1 == *s2))
		{
			s1++;
			s2++;
		}
		if (*s2 == '\0')
		{
			return (char*)cp;
		}
		cp++;
	}
	return NULL;
}
int main()
{
	char a[] = "asdaffd";
	char b[] = "ffd";
	char* c = my_strstr(a, b);
	if (c == NULL)
	{
		cout << "没找到" << endl;
	}
	else
	{
		cout << "找到了" << endl;
	}
	return 0;

}